FROM php:7.4-fpm

MAINTAINER Juan Blariza

#INSTALL DEPENDENCIES
RUN apt-get update && apt-get install -y \
    nginx

# Install composer
RUN php -r "readfile('https://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer

EXPOSE 9000

CMD ["php-fpm"]
