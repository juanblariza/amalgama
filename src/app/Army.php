<?php

namespace Amalgama\App;

use Amalgama\Common\Message;

class Army
{
    public $name;
    public $coins = 1000;
    public $units;
    private $civilization;

    public function __construct(string $name, Civilization $civilization)
    {
        $this->name = $name;
        $this->civilization = $civilization;
        $this->buildArmy();
        Message::info("Army {$name} created with {$this->coins} coins");
    }

    public function hasGoldAvailable(int $trainingCost)
    {
        return $this->coins >= $trainingCost;
    }

    public function substractCoins(int $coins)
    {
        $this->coins -= $coins;

        Message::info("Actual coins: {$this->coins}");
    }

    public function addCoins(int $coins)
    {
        $this->coins += $coins;

        Message::info("Added coins: {$coins}");
    }

    public function addUnit(Unit $unit)
    {
        $this->units[] = $unit;
        $unit->setArmy($this);
    }

    public function attack(Army $opponent)
    {
        Message::info("{$this->name} attacks to {$opponent->name}");

        if($this->isTie($opponent)) {
            Message::info("The battle was a tie");

            $this->removeUnitsWithMaxPoints(CONFIG['remove_units_by_tie']);
            $opponent->removeUnitsWithMaxPoints(CONFIG['remove_units_by_tie']);

            return null;
        }

        return $this->isWinner($opponent) ? $this->applyConsequences($opponent) : $opponent->applyConsequences($this);
    }

    private function applyConsequences(Army $loser)
    {
        Message::info("{$this->name} has won the battle");

        $this->addCoins(CONFIG['coins_won_for_winner']);
        $loser->removeUnitsWithMaxPoints(CONFIG['remove_units_for_loss']);

        return $this;
    }

    private function isTie(Army $opponent)
    {
        return $this->getTotalPoints() == $opponent->getTotalPoints();
    }

    private function removeUnitsWithMaxPoints(int $quantity): void
    {
        $unitsSorted = collect($this->units)->pluck('unitType.strengthPoints')->sort();
        $this->units = $unitsSorted->take(count($this->units) - $quantity);

        Message::info("{$this->name} has lost {$quantity} units");
    }

    private function isWinner(Army $opponent): bool
    {
        return $this->getTotalPoints() > $opponent->getTotalPoints();
    }

    private function getTotalPoints(): int
    {
        return collect($this->units)->sum(function($unit) {
            return $unit->unitType->strengthPoints;
        });
    }

    private function buildArmy(): void
    {
        $unitsType = [
            [
                'type' => UnitType::getKnightInstance(),
                'quantity' => $this->civilization->initialKnights
            ],
            [
                'type' => UnitType::getPikemenInstance(),
                'quantity' => $this->civilization->initialPikemen
            ],
            [
                'type' => UnitType::getArcherInstance(),
                'quantity' => $this->civilization->initialArchers
            ],
        ];

        foreach($unitsType as $unitType) {
            $this->addUnitsType($unitType['type'], $unitType['quantity']);
        }
    }

    private function addUnitsType(UnitType $unitType, $quantity): void
    {
        for($i = 0; $i < $quantity; $i++) {
            $this->addUnit(new Unit($unitType));
        }
    }
}