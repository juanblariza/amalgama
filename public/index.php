<?php

require '../vendor/autoload.php';

define('CONFIG', include('../config/app.php'));

use Amalgama\App\Army;
use Amalgama\App\Civilization\Byzantines;
use Amalgama\App\Civilization\Chinese;

if(!file_exists('../log/')) {
    mkdir('../log/', 0777, true);
}

/********************
 *  CHINESE ARMY  *
 *******************/

$chinese = new Chinese();
$chineseArmy = new Army('Chin', $chinese);

$chineseUnits = count($chineseArmy->units) - 1;

$chineseArmy->units[rand(0, $chineseUnits)]->train();
$chineseArmy->units[rand(0, $chineseUnits)]->train();
$chineseArmy->units[rand(0, $chineseUnits)]->train();
$chineseArmy->units[rand(0, $chineseUnits)]->train();
$chineseArmy->units[rand(0, $chineseUnits)]->train();
$chineseArmy->units[rand(0, $chineseUnits)]->transform();
$chineseArmy->units[rand(0, $chineseUnits)]->transform();

/********************
 *  BYZANTINE ARMY  *
 *******************/
$byzantines = new Byzantines();
$byzantinesArmy = new Army('Byz', $byzantines);

$byzantinesUnits = count($byzantinesArmy->units) - 1;

$byzantinesArmy->units[rand(0, $byzantinesUnits)]->train();
$byzantinesArmy->units[rand(0, $byzantinesUnits)]->train();
$byzantinesArmy->units[rand(0, $byzantinesUnits)]->train();
$byzantinesArmy->units[rand(0, $byzantinesUnits)]->train();
$byzantinesArmy->units[rand(0, $byzantinesUnits)]->train();
$byzantinesArmy->units[rand(0, $byzantinesUnits)]->train();
$byzantinesArmy->units[rand(0, $byzantinesUnits)]->train();
$byzantinesArmy->units[rand(0, $byzantinesUnits)]->train();
$byzantinesArmy->units[rand(0, $byzantinesUnits)]->transform();
$byzantinesArmy->units[rand(0, $byzantinesUnits)]->transform();
$byzantinesArmy->units[rand(0, $byzantinesUnits)]->transform();
$byzantinesArmy->units[rand(0, $byzantinesUnits)]->transform();
$byzantinesArmy->units[rand(0, $byzantinesUnits)]->transform();

//ATTACK
$chineseArmy->attack($byzantinesArmy);
