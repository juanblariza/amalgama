## AMALGAMA

## Setup

1. Clonar el proyecto `git clone https://gitlab.com/juanblariza/amalgama.git`
2. Sobre la raíz del projecto ejecutar:
    * `docker-compose up --build`
3. Ejecutar `composer dump-autoload -o`
3. La app escucha en el puerto 8000: `http://localhost:8000/` (se imprime las acciones que se van realizando en la app)


## Consideraciones

* En el archivo `index.php` que esta dentro de `public` hay ejemplos que se imprimen 
  en el browser como ser la creación de  Ejercitos con sus civilizaciones y unidades, 
  y los entrenamientos y transformaciones de cada unidad.
 